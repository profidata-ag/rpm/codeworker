Name:           codeworker
Version:        4.5.4
Release:        3%{?dist}
Summary:        A universal parsing tool and a source code generator
License:        LGPLv2+
URL:            http://codeworker.free.fr/
Source0:        https://gitlab.com/profidata-ag/tools/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  ncurses-devel
BuildRequires:  readline-devel

%description
CodeWorker is a versatile parsing tool and a source code generator
devoted to generative programming. Generative programming is a
software engineering approach interested in automating the production
of reusable, tailor-made, adaptable and reliable IT systems.
In layman's terms, CodeWorker lets you generate code by parsing
existing languages, or by creating and parsing your own language. Once
a language file has been parsed, CodeWorker provides several
techniques for generating code.

%package devel
Summary: Codeworker static library
Requires: %{name} = %{version}

%description devel
This package include the static library.

%package extensions
Summary: Extensions used for XENTIS development
Requires: %{name} = %{version}

%description extensions
This package include XENTIS specific extensions.

%prep
%autosetup

%build
%cmake
%cmake_build

%install
%cmake_install

%files
%{_bindir}/codeworker

%files devel
%{_includedir}/CW4dl.h

%files extensions
%{_libdir}/XENTIScw.so


%changelog
* Mon Feb 21 2022 Milivoje Legenovic (milivoje.legenovic@profidata.com) - 4.5.4-3
- xdev-6 build
